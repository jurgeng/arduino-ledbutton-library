#ifndef MY_LEDBUTTON_H
#define MY_LEDBUTTON_H

#include <Arduino.h>

class LedButton{
  
  private:
    byte ledPin;
    byte buttonPin;
    byte buttonState;
    byte ledState;
    byte lastReading;
    unsigned long lastDebounceTime = 0;
    unsigned long debounceDelay = 50;

  public:
    LedButton(byte ledPin, byte buttonPin) ;
    void init();
    void on();
    void off();
    bool ledOn();
    void update();
    byte getButtonState();
    bool isPressed();
    void toggle();
};

#endif
