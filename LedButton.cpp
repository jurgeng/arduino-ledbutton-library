#include "LedButton.h"

// CREDENTIALS - blatantly copied from this page
// https://roboticsbackend.com/arduino-object-oriented-programming-oop/


LedButton::LedButton(byte ledPin, byte buttonPin) {
      this->ledPin = ledPin;
      this->buttonPin = buttonPin;
      lastReading = HIGH;
      init();
    }

void LedButton::init() {
      pinMode(ledPin, OUTPUT);
      pinMode(buttonPin, INPUT_PULLUP);
      off();
      update();
    }

void LedButton::on() {
      digitalWrite(ledPin, HIGH);
    }

void LedButton::off() {
      digitalWrite(ledPin, LOW);
    }

bool LedButton::ledOn() {
      return (ledState==HIGH);
    }

void LedButton::update() {
      byte newReading = digitalRead(buttonPin);

      if(newReading != lastReading) {
        lastDebounceTime = millis();
      }
      
      if (millis() - lastDebounceTime > debounceDelay) {
        buttonState = newReading;
      }

      lastReading = newReading;
    }

byte LedButton::getButtonState() {
      update();
      return buttonState;
    }

bool LedButton::isPressed() {
      return (getButtonState() == LOW);
    }

void LedButton::toggle() {
  if(ledOn()) {
    off();
  } else {
    on();
  }
  
}
